package com.example.duplicates;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class FailingComponent {
    Logger logger = LoggerFactory.getLogger(FailingComponent.class);

    // just to show that it can fail later on at runtime and not necessarily at startup
    @Scheduled(fixedDelay = 5000)
    public void test(){
        // create a random instance from a class from the web3j core module
        org.web3j.protocol.exceptions.ClientConnectionException shouldWork = new org.web3j.protocol.exceptions.ClientConnectionException("should work");

        // create a random instance from a class from the zxing core module
        com.google.zxing.Dimension shouldAlsoWork = new com.google.zxing.Dimension(10,10);

        logger.info("Went fine...");

    }
}
