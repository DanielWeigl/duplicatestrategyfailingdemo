package com.example.duplicates;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class DuplicatesApplication {

    public static void main(String[] args) {
        SpringApplication.run(DuplicatesApplication.class, args);
    }

}
