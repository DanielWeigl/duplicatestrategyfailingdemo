# Colliding filenames in jar/war files


These two unrelated dependencies contain accidentally two different modules with the same filename:
 - `org.web3j:core:3.4.0`
 - `com.google.zxing:core:3.4.0` 
 
Which in itself is perfectly fine.


But if we package them in a .jar or .war file (via the BootJar/BootWar Task), it generates a zip file with 
colliding filenames and the class-loader only loads one of the modules and fails at runtime if the code
references a class from the other file.


Starting it in Intellij:

```

  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::        (v2.2.6.RELEASE)

2020-04-23 09:14:33.106  INFO 558 --- [           main] c.e.duplicates.DuplicatesApplication     : Starting DuplicatesApplication on danielnb with PID 558 (/home/daniel/src/duplicates/build/classes/java/main started by daniel in /home/daniel/src/duplicates)
2020-04-23 09:14:33.109  INFO 558 --- [           main] c.e.duplicates.DuplicatesApplication     : No active profile set, falling back to default profiles: default
2020-04-23 09:14:33.560  INFO 558 --- [           main] o.s.s.c.ThreadPoolTaskScheduler          : Initializing ExecutorService 'taskScheduler'
2020-04-23 09:14:33.575  INFO 558 --- [   scheduling-1] com.example.duplicates.FailingComponent  : Went fine...
2020-04-23 09:14:33.577  INFO 558 --- [           main] c.e.duplicates.DuplicatesApplication     : Started DuplicatesApplication in 0.75 seconds (JVM running for 1.503)
2020-04-23 09:14:38.575  INFO 558 --- [   scheduling-1] com.example.duplicates.FailingComponent  : Went fine...
2020-04-23 09:14:43.576  INFO 558 --- [   scheduling-1] com.example.duplicates.FailingComponent  : Went fine...
```

Starting it via BootJar:

```
> ./gradlew --version                                                  

------------------------------------------------------------
Gradle 6.3
------------------------------------------------------------

Build time:   2020-03-24 19:52:07 UTC
Revision:     bacd40b727b0130eeac8855ae3f9fd9a0b207c60

Kotlin:       1.3.70
Groovy:       2.5.10
Ant:          Apache Ant(TM) version 1.10.7 compiled on September 1 2019
JVM:          1.8.0_242 (Private Build 25.242-b08)
OS:           Linux 5.3.0-42-generic amd64


# builds without warning:
> ./gradlew bootJar --warning-mode all                 

BUILD SUCCESSFUL in 752ms
3 actionable tasks: 3 up-to-date

# verifiy if there are colliding filenames: yes
> unzip -l ./build/libs/duplicates-0.0.1-SNAPSHOT.jar | grep core-3.4.0
warning [./build/libs/duplicates-0.0.1-SNAPSHOT.jar]:  9093 extra bytes at beginning or within zipfile
  (attempting to process anyway)
   244695  2019-12-10 08:48   BOOT-INF/lib/core-3.4.0.jar
   539901  2020-03-31 13:10   BOOT-INF/lib/core-3.4.0.jar

# run it 
> ./build/libs/duplicates-0.0.1-SNAPSHOT.jar


 .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::        (v2.2.6.RELEASE)

2020-04-23 09:16:11.629  INFO 928 --- [           main] c.e.duplicates.DuplicatesApplication     : Starting DuplicatesApplication on danielnb with PID 928 (/home/daniel/src/duplicates/build/libs/duplicates-0.0.1-SNAPSHOT.jar started by daniel in /home/daniel/src/duplicates/build/libs)
2020-04-23 09:16:11.632  INFO 928 --- [           main] c.e.duplicates.DuplicatesApplication     : No active profile set, falling back to default profiles: default
2020-04-23 09:16:12.118  INFO 928 --- [           main] o.s.s.c.ThreadPoolTaskScheduler          : Initializing ExecutorService 'taskScheduler'
2020-04-23 09:16:12.138 ERROR 928 --- [   scheduling-1] o.s.s.s.TaskUtils$LoggingErrorHandler    : Unexpected error occurred in scheduled task

java.lang.NoClassDefFoundError: org/web3j/protocol/exceptions/ClientConnectionException
        at com.example.duplicates.FailingComponent.test(FailingComponent.java:18) ~[classes!/:na]
        at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method) ~[na:1.8.0_242]
        at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62) ~[na:1.8.0_242]
        at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43) ~[na:1.8.0_242]
        at java.lang.reflect.Method.invoke(Method.java:498) ~[na:1.8.0_242]
        at org.springframework.scheduling.support.ScheduledMethodRunnable.run(ScheduledMethodRunnable.java:84) ~[spring-context-5.2.5.RELEASE.jar!/:5.2.5.RELEASE]
        at org.springframework.scheduling.support.DelegatingErrorHandlingRunnable.run(DelegatingErrorHandlingRunnable.java:54) ~[spring-context-5.2.5.RELEASE.jar!/:5.2.5.RELEASE]
        at java.util.concurrent.Executors$RunnableAdapter.call(Executors.java:511) [na:1.8.0_242]
        at java.util.concurrent.FutureTask.runAndReset(FutureTask.java:308) [na:1.8.0_242]
        at java.util.concurrent.ScheduledThreadPoolExecutor$ScheduledFutureTask.access$301(ScheduledThreadPoolExecutor.java:180) [na:1.8.0_242]
        at java.util.concurrent.ScheduledThreadPoolExecutor$ScheduledFutureTask.run(ScheduledThreadPoolExecutor.java:294) [na:1.8.0_242]
        at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1149) [na:1.8.0_242]
        at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624) [na:1.8.0_242]
        at java.lang.Thread.run(Thread.java:748) [na:1.8.0_242]
Caused by: java.lang.ClassNotFoundException: org.web3j.protocol.exceptions.ClientConnectionException
        at java.net.URLClassLoader.findClass(URLClassLoader.java:382) ~[na:1.8.0_242]
        at java.lang.ClassLoader.loadClass(ClassLoader.java:419) ~[na:1.8.0_242]
        at org.springframework.boot.loader.LaunchedURLClassLoader.loadClass(LaunchedURLClassLoader.java:92) ~[duplicates-0.0.1-SNAPSHOT.jar:na]
        at java.lang.ClassLoader.loadClass(ClassLoader.java:352) ~[na:1.8.0_242]
        ... 14 common frames omitted

2020-04-23 09:16:12.138  INFO 928 --- [           main] c.e.duplicates.DuplicatesApplication     : Started DuplicatesApplication in 0.834 seconds (JVM running for 1.235)
2020-04-23 09:16:17.139 ERROR 928 --- [   scheduling-1] o.s.s.s.TaskUtils$LoggingErrorHandler    : Unexpected error occurred in scheduled task

java.lang.NoClassDefFoundError: org/web3j/protocol/exceptions/ClientConnectionException
        at com.example.duplicates.FailingComponent.test(FailingComponent.java:18) ~[classes!/:na]
        at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method) ~[na:1.8.0_242]
        at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62) ~[na:1.8.0_242]
        at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43) ~[na:1.8.0_242]
        at java.lang.reflect.Method.invoke(Method.java:498) ~[na:1.8.0_242]
        at org.springframework.scheduling.support.ScheduledMethodRunnable.run(ScheduledMethodRunnable.java:84) ~[spring-context-5.2.5.RELEASE.jar!/:5.2.5.RELEASE]
        at org.springframework.scheduling.support.DelegatingErrorHandlingRunnable.run(DelegatingErrorHandlingRunnable.java:54) ~[spring-context-5.2.5.RELEASE.jar!/:5.2.5.RELEASE]
        at java.util.concurrent.Executors$RunnableAdapter.call(Executors.java:511) [na:1.8.0_242]
        at java.util.concurrent.FutureTask.runAndReset(FutureTask.java:308) [na:1.8.0_242]
        at java.util.concurrent.ScheduledThreadPoolExecutor$ScheduledFutureTask.access$301(ScheduledThreadPoolExecutor.java:180) [na:1.8.0_242]
        at java.util.concurrent.ScheduledThreadPoolExecutor$ScheduledFutureTask.run(ScheduledThreadPoolExecutor.java:294) [na:1.8.0_242]
        at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1149) [na:1.8.0_242]
        at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:624) [na:1.8.0_242]
        at java.lang.Thread.run(Thread.java:748) [na:1.8.0_242]

^C2020-04-23 09:16:17.915  INFO 928 --- [extShutdownHook] o.s.s.c.ThreadPoolTaskScheduler          : Shutting down ExecutorService 'taskScheduler'
```

It sometimes fails with a `NoClassDefFoundError` from one or the other `core-3.4.0` module, I guess depending on the order in the jar file